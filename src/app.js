
import './app.css'

import 'bootstrap/dist/css/bootstrap.min.css'

import { createApp } from 'vue'
import app from './app.vue'

createApp(app).mount('#app')
