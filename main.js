
const { app, BrowserWindow } = require('electron')

function createWindow() {
    const options = {
        width: 360,
        height: 600,
        webPreferences: { nodeIntegration: true }
    }
    const win = new BrowserWindow(options)
    win.loadFile('./dist/index.html')
}

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
})
